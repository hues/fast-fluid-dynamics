# README #

### General description

Fast Fluid Dynamics (FFD).
Implementation in Processing.

![](https://bytebucket.org/hues/fast-fluid-dynamics/raw/541fa765f15e628da2479a4e616e7b3e2fa3a763/ffd2.gif)

### More information

Fast Fluid Dynamics (FFD) based on Jos Stam (1999) - Stable Fluids.
This implementation in Java / Processing includes the FFD solver as well as additions to internal boundaries, pressure calculations and visualization.
FFD solver is based on Mike Ash's Tutorial: https://mikeash.com/pyblog/fluid-simulation-for-dummies.html

The required (outdated) Processing version 1.5.1 can be downloaded on this repository as well.
All required Processing libraries are included in the folder "lib".

The implementation (EDEMScFastFluidDynamics005.java) is actually written in Eclipse (using ProClipsing), but with some adjustments also works directly in Processing.
This code is part of the MSc thesis "Non-deterministic shape optimisation of wind-cowls by applying simulated annealing and fast fluid dynamics": http://nceub.org.uk/mc2012/pdfs/mc12-33_waibel.pdf


### Authors

Christoph Waibel

### License / Software used

https://mikeash.com/pyblog/fluid-simulation-for-dummies.html

https://processing.org/

http://toxiclibs.org/

http://mrfeinberg.com/peasycam/

http://www.sojamo.de/libraries/controlP5/